const path = require('path')

const data = require('./csv-to-array.cjs')

function matchesWonPerTeamPerYear(cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const results = matchesArray.reduce((accu, match) => {
            if(accu[match.season] === undefined) {
                accu[match.season] = {}
            }
            if(accu[match.season][match.winner] === undefined && match.result !== "no result") {
                accu[match.season][match.winner] = 0
            }
            if( match.result !== "no result") {
                accu[match.season][match.winner]++
            }

            return accu
        },{})

        cb(results)
    })
}

module.exports = matchesWonPerTeamPerYear
