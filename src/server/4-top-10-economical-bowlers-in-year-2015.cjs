const path = require('path')

const data = require('./csv-to-array.cjs')

function topTenEcoBowlerYear(year = "2015", cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const matchIds = matchesArray.reduce((accu, match) => {
            if (match.season === year) {
                accu.push(match.id)
            }
            return accu
        }, [])

        data(path.join(__dirname,'..','data/deliveries.csv'), (deliveries) => {
            let results = deliveries.reduce((accu, delivery) => {
                if (matchIds.includes(delivery.match_id)) {
                    if(accu[delivery.bowler] === undefined) {
                        accu[delivery.bowler] = {runs: 0, balls: 0, eco: 0}
                    }
                    accu[delivery.bowler].runs += Number.parseInt(delivery.total_runs)
                    if(delivery.wide_runs === '0' && delivery.noball_runs === '0'){
                        accu[delivery.bowler].balls++
                    }
                }
                return accu
            },{})

            results = Object.entries(results).reduce((accu, playerEntry) => {
                const playerObj = {}
                playerObj[playerEntry[0]] = playerEntry[1].runs / Math.floor(playerEntry[1].balls / 6)
                accu.push(playerObj)
                return accu
            },[])
            .sort((player1, player2) => {
                // player1 and player2 are object, where playername is key and value is eco rate
                return Object.values(player1) - Object.values(player2)
            })

            cb(results.slice(0,10))
        })
    })
}

module.exports = topTenEcoBowlerYear