const path = require('path')

const data = require('./csv-to-array.cjs')

function strikeRatePerSeason(cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const years = matchesArray.reduce((accu, match) => {
            accu[match.id] = match.season
            return accu
        }, {})

        data(path.join(__dirname,'..','data/deliveries.csv'), (deliveries) => {
            let results = deliveries.reduce((accu, delivery) => {
                if (accu[years[delivery.match_id]] === undefined) {
                    accu[years[delivery.match_id]] = {}
                }
                if (accu[years[delivery.match_id]][delivery.batsman] === undefined) {
                    accu[years[delivery.match_id]][delivery.batsman] = { runs: 0, balls: 0, strike: 0 }
                }
                accu[years[delivery.match_id]][delivery.batsman].runs += Number.parseInt(delivery.batsman_runs)
                if (delivery.wide_runs === '0' && delivery.noball_runs === '0') {
                    accu[years[delivery.match_id]][delivery.batsman].balls++
                }
                accu[years[delivery.match_id]][delivery.batsman].strike =
                    (accu[years[delivery.match_id]][delivery.batsman].runs /
                        accu[years[delivery.match_id]][delivery.batsman].balls) * 100

                return accu
            }, {})

            cb(results)
        })
    })
}

module.exports = strikeRatePerSeason