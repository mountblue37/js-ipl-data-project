const path = require('path')

const data = require('./csv-to-array.cjs')

function timeTeamsWonTossAndMatch(cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const results = matchesArray.reduce((accu, match) => {
            if(accu[match.toss_winner] === undefined) {
                accu[match.toss_winner] = 0
            }
            if(match.toss_winner === match.winner) {
                accu[match.toss_winner]++
            }
            
            return accu
        },{})

        cb(results)
    })
}

module.exports = timeTeamsWonTossAndMatch
