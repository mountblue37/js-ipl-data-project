const fs = require('fs')

const { parse } = require('csv-parse')

function csvToArray(src = "", cb) {
    const results = []

    fs.createReadStream(src)
    .pipe(parse({
        columns: true
    }))
    .on('data', (data) => {
       results.push(data)
    })
    .on('error', (err) => {
        console.log(err)
    })
    .on('end', () => {
       cb(results)
    })
}

module.exports = csvToArray