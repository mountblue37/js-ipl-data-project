const path = require('path')

const data = require('./csv-to-array.cjs')

function extraRunsConceded(year = '2016', cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const matchIds = matchesArray.reduce((accu, match) => {
            if (match.season === year) {
                accu.push(match.id)
            }
            return accu
        }, [])

        data(path.join(__dirname,'..','data/deliveries.csv'), (deliveries) => {
            const results = deliveries.reduce((accu, delivery) => {
                if (matchIds.includes(delivery.match_id)) {
                    if (accu[delivery.bowling_team] === undefined) {
                        accu[delivery.bowling_team] = 0
                    }
                    accu[delivery.bowling_team] += Number.parseInt(delivery.extra_runs)
                }
                return accu
            }, {})

            cb(results)
        })
    })
}

module.exports = extraRunsConceded