const path = require('path')

const data = require('./csv-to-array.cjs')

function highestPlayerOfMatch(cb) {
    data(path.join(__dirname, '..', 'data/matches.csv'), (matchesArray) => {
        let results = matchesArray.reduce((accu, match) => {
            if(match.player_of_match !== "") {
                if (accu[match.season] === undefined) {
                    accu[match.season] = { max: {} }
                    accu[match.season].max[match.player_of_match] = 0
                }
                if (accu[match.season][match.player_of_match] === undefined) {
                    accu[match.season][match.player_of_match] = 0
                }
            
                accu[match.season][match.player_of_match]++
            
                if (accu[match.season][match.player_of_match] > Object.values(accu[match.season].max)) {
                    accu[match.season].max = {}
                    accu[match.season].max[match.player_of_match] = accu[match.season][match.player_of_match]
                }    
            }
            
            return accu
        }, {})

        results = Object.keys(results).reduce((accu, year) => {
            accu[year] = results[year].max
            return accu
        }, results)

        cb(results)
    })
}

module.exports = highestPlayerOfMatch