const path = require('path')

const data = require('./csv-to-array.cjs')

function highestTimesDismissed(cb) {
    data(path.join(__dirname,'..','data/deliveries.csv'), (deliveries) => {
        let results = deliveries.reduce((accu, delivery) => {
            if (delivery.player_dismissed !== "") {
                if (accu[delivery.player_dismissed] === undefined) {
                    accu[delivery.player_dismissed] = {}
                    if (accu[delivery.player_dismissed]['max'] === undefined) {
                        accu[delivery.player_dismissed]['max'] = {}
                        accu[delivery.player_dismissed]['max'][delivery.bowler] = { times_dismmised: 0, total_runs: Number.MAX_SAFE_INTEGER }
                    }
                }
                if (accu[delivery.player_dismissed][delivery.bowler] === undefined) {
                    accu[delivery.player_dismissed][delivery.bowler] = { times_dismmised: 0, total_runs: 0 }
                }
                accu[delivery.player_dismissed][delivery.bowler].times_dismmised++
                accu[delivery.player_dismissed][delivery.bowler].total_runs += Number.parseInt(delivery.total_runs)


                if (Object.values(accu[delivery.player_dismissed]['max'])[0].times_dismmised < accu[delivery.player_dismissed][delivery.bowler].times_dismmised) {
                    accu[delivery.player_dismissed].max = {}
                    accu[delivery.player_dismissed].max[delivery.bowler] = accu[delivery.player_dismissed][delivery.bowler]
                } else if (Object.values(accu[delivery.player_dismissed]['max'])[0].times_dismmised === accu[delivery.player_dismissed][delivery.bowler].times_dismmised) {
                    // if the times dismissed is same we select the bowlere who has give least number of runs
                    if (Object.values(accu[delivery.player_dismissed]['max'])[0].total_runs < accu[delivery.player_dismissed][delivery.bowler].total_runs) {
                        accu[delivery.player_dismissed].max = {}
                        accu[delivery.player_dismissed].max[delivery.bowler] = accu[delivery.player_dismissed][delivery.bowler]
                    }
                }
            }

            return accu
        }, {})

        results = Object.keys(results).reduce((accu, batsman) => {
            accu[batsman] = accu[batsman].max
            return accu
        }, results)

        cb(results)
    })
}

module.exports = highestTimesDismissed