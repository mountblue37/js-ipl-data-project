const path = require('path')

const data = require('./csv-to-array.cjs')

function matchesPerYear(cb) {
    data(path.join(__dirname,'..','data/matches.csv'), (matchesArray) => {
        const results = matchesArray.reduce((accu, match) => {
            if (accu[match.season] === undefined) {
                accu[match.season] = 0
            }
            accu[match.season]++
            return accu
        }, {})

        cb(results)
    })
}

module.exports = matchesPerYear