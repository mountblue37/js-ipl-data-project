const path = require('path')

const express = require('express')

const PORT = process.env.PORT || 3000

const app = express()

app.use(express.static('src/public/'))

app.get('/problem1', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '1-matches-per-year.json'))
})

app.get('/problem2', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '2-matches-won-per-team-per-year.json'))
})

app.get('/problem3', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '3-extra-runs-conceded-per-team-in-2016.json'))
})

app.get('/problem4', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '4-top-10-economical-bowlers-in-year-2015.json'))
})

app.get('/problem5', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '5-number-of-times-each-team-won-toss-and-match.json'))
})

app.get('/problem6', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '6-player-with-highest-number-of-player-of-the-match-per-season.json'))
})

app.get('/problem7', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '7-strike-rate-of-a-batsman-per-season.json'))
})

app.get('/problem8', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json'))
})

app.get('/problem9', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'output', '9-the-bowler-with-the-best-economy-in-super-overs.json'))
})

app.get('/signup', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'views', 'signup.html'))
})

app.get('/links', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'views', 'problem-links.html'))
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'views', 'landing.html'))
})


app.listen(PORT, () => {
    console.log(`Server is live on port ${PORT}`)
})