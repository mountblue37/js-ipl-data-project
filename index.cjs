const fs = require('fs')
const path = require('path')

const matchesPeryear = require('./src/server/1-matches-per-year.cjs')
const matchesWonPerTeam = require('./src/server/2-matches-won-per-team-per-year.cjs')
const extraRunsConceded = require('./src/server/3-extra-runs-conceded-per-team-in-2016.cjs')
const topTenEcoBowlers = require('./src/server/4-top-10-economical-bowlers-in-year-2015.cjs')
const timesTeamWonTossAndMatch = require('./src/server/5-number-of-times-each-team-won-toss-and-match.cjs')
const playerWithMostPOM = require('./src/server/6-player-with-highest-number-of-player-of-the-match-per-season.cjs')
const strikeRatePerSeason = require('./src/server/7-strike-rate-of-a-batsman-per-season.cjs')
const numberOfTimesPlayerDismissed = require('./src/server/8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.cjs')
const bestEcoInSuper = require('./src/server/9-the-bowler-with-the-best-economy-in-super-overs.cjs')

// matchesPeryear((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '1-matches-per-year.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// matchesWonPerTeam((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '2-matches-won-per-team-per-year.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// extraRunsConceded('2016', (results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '3-extra-runs-conceded-per-team-in-2016.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// topTenEcoBowlers('2015', (results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '4-top-10-economical-bowlers-in-year-2015.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// timesTeamWonTossAndMatch((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '5-number-of-times-each-team-won-toss-and-match.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// playerWithMostPOM((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '6-player-with-highest-number-of-player-of-the-match-per-season.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// strikeRatePerSeason((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '7-strike-rate-of-a-batsman-per-season.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })  

// numberOfTimesPlayerDismissed((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// })

// bestEcoInSuper((results) => {
//     console.log(results)
//     fs.writeFile(path.join(__dirname, 'src/public/output', '9-the-bowler-with-the-best-economy-in-super-overs.json'), JSON.stringify(results), 'utf-8', (err) => {
//         if (err === null) {
//             console.log('written successfully')
//         } else {
//             console.log(err)
//         }
//     })
// }) 